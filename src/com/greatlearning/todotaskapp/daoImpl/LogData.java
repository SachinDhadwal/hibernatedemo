package com.greatlearning.todotaskapp.daoImpl;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class LogData {

    public void storeData(String msg)  {
        Path path = Paths.get("MyLogFile.txt");
        Date date = new Date();
        String logDate = date.dateFormatter();
        String result =  logDate.concat(msg+"\n");
        boolean fileExits = Files.exists(path);
        try {
            if(fileExits) {
                Files.write(path, result.getBytes(), StandardOpenOption.APPEND);
            }
            else {
                Files.createFile(path);
                Files.write(path,result.getBytes());
            }
        } catch (IOException exception){
            exception.printStackTrace();
        }


    }
}
