package com.greatlearning.todotaskapp.daoImpl;

import com.greatlearning.todotaskapp.customException.InvalidUserException;
import com.greatlearning.todotaskapp.model.Client;
import com.greatlearning.todotaskapp.model.Visitor;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

public class Login {
    public Login() {
        AppConst.configuration = new Configuration();
        AppConst.configuration.configure("com/greatlearning/todotaskapp/hibernate.cfg.xml");
        AppConst.factory = AppConst.configuration.buildSessionFactory();
    }
    public void doLogin(String username, String password) throws InvalidUserException {
        Session session = AppConst.factory.openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery("from Client where username=:username and password=:password");
        query.setParameter("username",username);
        query.setParameter("password",password);
        Client client = (Client) query.uniqueResult();
        transaction.commit();
        if(client!=null){
            System.out.println("username and Password are valid");
        }
        else {
            throw new InvalidUserException("UserName password not matched");
        }
        session.close();
    }

    public void visitorLogin(String username, String password) throws InvalidUserException {
        Session session = AppConst.factory.openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery("from visitor where username=:username and password=:password");
        query.setParameter("username",username);
        query.setParameter("password",password);
        Visitor visitor = (Visitor) query.uniqueResult();
        transaction.commit();
        if(visitor!=null){
            System.out.println("username and Password are valid");
        }
        else {
            throw new InvalidUserException("UserName password not matched");
        }
        session.close();
    }


}
