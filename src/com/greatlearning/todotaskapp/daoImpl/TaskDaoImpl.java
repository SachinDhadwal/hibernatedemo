package com.greatlearning.todotaskapp.daoImpl;

import com.greatlearning.todotaskapp.dao.TaskDao;
import com.greatlearning.todotaskapp.model.Tasks;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.Iterator;
import java.util.List;

public class TaskDaoImpl  implements TaskDao {


     Tasks tasks = new Tasks();

    public TaskDaoImpl() {
        AppConst.configuration = new Configuration();
        AppConst.configuration.configure("com/greatlearning/todotaskapp/hibernate.cfg.xml");
        AppConst.factory = AppConst.configuration.buildSessionFactory();
    }

    @Override
    public void add(long taskId, String taskTitle, String username) {
     tasks.setTaskId(taskId);
     tasks.setTaskname(taskTitle);
     tasks.setUsername(username);
        Session session = AppConst.factory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(tasks);
        transaction.commit();
        System.out.println("Task added");
        session.close();


    }

    @Override
    public void delete(long taskId) {
        Session session = null;
        session = AppConst.factory.openSession();
        session.beginTransaction();
        Tasks tasks1 = session.get(Tasks.class,taskId);
        session.delete(tasks1);
        System.out.println("Record Deleted");
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void updateTask(long taskId) {
       Session session = null;
       try {
           session = AppConst.factory.openSession();
           Transaction transaction = session.beginTransaction();
           Tasks tasks1 = session.get(Tasks.class,taskId);
           System.out.println("Enter the task name");
           String taskname = AppConst.scanner.next();
           tasks1.setTaskname(taskname);
           session.update(tasks1);
           System.out.println("task updated successfully");
           transaction.commit();
           session.close();
       } catch (Exception exception){
           if(null!=session.getTransaction()){
               session.getTransaction().rollback();
           }
       }
    }

    @Override
    public Tasks searchTask(long taskId) {
        Session  session = AppConst.factory.openSession();
        Transaction transaction = session.beginTransaction();
        Tasks tasks1 = session.get(Tasks.class,taskId);
        transaction.commit();
        session.close();
        return tasks1;
    }

    @Override
    public void assignTask(long taskId, String username) {
     Session session = AppConst.factory.openSession();
     Transaction transaction = session.beginTransaction();
     Tasks tasks1 = searchTask(taskId);
     tasks1.setUsername(username);
     session.update(tasks1);
        System.out.println("Task Asssigned to"+username);
        transaction.commit();
        session.close();
    }

    @Override
    public void listTasks() {
        Session session = AppConst.factory.openSession();
        Transaction transaction = session.beginTransaction();
        Criteria  criteria = session.createCriteria(Tasks.class);
        List taskList = criteria.list();
        Iterator iterator = taskList.iterator();
        while (iterator.hasNext()) {
            Tasks tasks1 = (Tasks) iterator.next();
            System.out.println(tasks1.getTaskId()+"   "+tasks1.getTaskname());
        }

    }

     public void listAllTasks(String username) {
         Session session = AppConst.factory.openSession();
         Transaction transaction = session.beginTransaction();
         Query query = session.createQuery("From Tasks t where t.username=:username");
         query.setParameter("username",username);
         List result = query.list();
         Iterator iterator = result.iterator();
         while (iterator.hasNext()){
             Tasks tasks1 = (Tasks) iterator.next();
             System.out.println(tasks1.getTaskname());
         }
         transaction.commit();
         session.close();
     }



}
