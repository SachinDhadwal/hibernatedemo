package com.greatlearning.todotaskapp.daoImpl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Date {

    public String dateFormatter() {
        LocalDateTime current = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        return current.format(formatter);
    }
}
