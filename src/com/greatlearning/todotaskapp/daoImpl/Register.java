package com.greatlearning.todotaskapp.daoImpl;

import com.greatlearning.todotaskapp.model.Client;
import com.greatlearning.todotaskapp.model.Visitor;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Register {

    Client client;
    Visitor visitor;

    public Register() {
        AppConst.configuration = new Configuration();
        AppConst.configuration.configure("com/greatlearning/todotaskapp/hibernate.cfg.xml");
        AppConst.factory = AppConst.configuration.buildSessionFactory();
    }

    public void doRegister(String username,String password,String type){
        client = new Client();
        client.setUserName(username);
        client.setPassword(password);
        client.setUserType(type);
        Session session = AppConst.factory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(client);
        transaction.commit();
        System.out.println("New user Register Successfully !!!");
        session.close();
    }

    public void visitorRegister(String username,String password,String type) {
         visitor = new Visitor();
         visitor.setUserName(username);
         visitor.setPassword(password);
         visitor.setUserType(type);
        Session session = AppConst.factory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(visitor);
        transaction.commit();
        System.out.println("New Visitor Register Successfully !!!");
        session.close();
    }
}
