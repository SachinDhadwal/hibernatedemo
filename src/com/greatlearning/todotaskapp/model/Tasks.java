package com.greatlearning.todotaskapp.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity

public class Tasks {

    @Id
    private long taskId;
    private String taskname;
    private String username;

    public Tasks() {
    }

    public Tasks(long taskId, String taskname, String username) {
        this.taskId = taskId;
        this.taskname = taskname;
        this.username = username;
    }

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public String getTaskname() {
        return taskname;
    }

    public void setTaskname(String taskname) {
        this.taskname = taskname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "Tasks{" +
                "taskId=" + taskId +
                ", taskname='" + taskname + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
