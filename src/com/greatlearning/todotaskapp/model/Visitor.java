package com.greatlearning.todotaskapp.model;

import javax.persistence.*;

@Entity
@Table(name = "visitor")
public class Visitor {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long  clientId;
    @Column(name = "username",unique = true)
    private String userName;
    private String password;
    private String userType;

    public Visitor() {
    }

    public Visitor(long clientId, String userName, String password, String userType) {
        this.clientId = clientId;
        this.userName = userName;
        this.password = password;
        this.userType = userType;
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    @Override
    public String toString() {
        return "Visitor{" +
                "clientId=" + clientId +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", userType='" + userType + '\'' +
                '}';
    }
}
