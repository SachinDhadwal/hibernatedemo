package com.greatlearning.todotaskapp;

import com.greatlearning.todotaskapp.customException.InvalidUserException;
import com.greatlearning.todotaskapp.daoImpl.*;
import com.greatlearning.todotaskapp.model.Client;
import com.greatlearning.todotaskapp.model.Tasks;

import java.util.ArrayList;
import java.util.List;

public class Main {

    static List<String> list = new ArrayList<>();
    static LogData logData = new LogData();
    static  int clientType;
    static String userName;
    public static void main(String[] args) throws InterruptedException, InvalidUserException {
     list.add("1. Add a task");
     list.add("2. Update a task");
     list.add("3. Search a task");
     list.add("4. Delete a task");
     list.add("5. Assign a task");
     list.add("6. List all tasks");
     list.add("7. List all assigned tasks");
     list.add("Enter your choice");


     do {
         System.out.println("Hello");
         System.out.println("Press 1 if you are client");
         System.out.println("Press 2 if you are visitor");
         clientType = AppConst.scanner.nextInt();
         if(clientType==1) {
             Thread client = new Thread(Main::client);
             client.start();
             client.join();
         }
         else if(clientType==2) {
             visitor();
         }
         else {
             System.out.println("incorret Option");
         }
         System.out.println("Do you want to continue");
     } while (AppConst.scanner.nextInt()!=0);
    }
    public static void client() {
        try {
            Client client = loginMenu();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }


    public static Client loginMenu() {
        System.out.println("1. Login");
        System.out.println("2. Register");
        int choice = AppConst.scanner.nextInt();
        String name, password, type;
        TaskDaoImpl dao = new TaskDaoImpl();
        Client  client = null;
        switch (choice) {
            case 1 :
                System.out.println("Enter the username");
                name = AppConst.scanner.next();
                System.out.println("enter the password");
                password = AppConst.scanner.next();
                userName = name;
                Login login = new Login();
                try {
                    login.doLogin(name,password);
                } catch (InvalidUserException e) {
                    System.out.println("Wrong user name");
                    System.exit(0);
                }

                do {
                   list.forEach(item -> System.out.println(item));
                        int option = AppConst.scanner.nextInt();
                        long taskId;
                        switch (option) {
                            case 1:
                                System.out.println("enter the task Id");
                                taskId = AppConst.scanner.nextInt();
                                System.out.println("Enter task name");
                                String taskName = AppConst.scanner.next();
                                ;
                                dao.add(taskId,taskName,userName);
                                logData.storeData("add method called");
                               break;
                            case 2:
                                System.out.println("Enter the task Id");
                                taskId =  AppConst.scanner.nextInt();
                                dao.updateTask(taskId);
                                logData.storeData("update method called");
                                break;
                            case 3:
                                System.out.println("Enter the task Id");
                                    taskId = AppConst.scanner.nextInt();
                                Tasks tasks1 = dao.searchTask(taskId);
                                System.out.println(tasks1.getTaskId()+"  "+ tasks1.getTaskname());
                                logData.storeData("search method called");
                                break;

                            case 4:
                                System.out.println("Enter the task Id");
                                taskId = AppConst.scanner.nextInt();
                                dao.delete(taskId);
                                System.out.println("Task ID"+ taskId+"deleted");
                                logData.storeData("delete method called");
                                break;

                            case 5:
                                System.out.println("Enter the task Id");
                                taskId = AppConst.scanner.nextInt();
                                dao.assignTask(taskId,userName);
                                logData.storeData("assign method called");
                                break;
                            case 6:
                                dao.listTasks();
                                logData.storeData("list task method called");
                                break;
                            case 7:
                                System.out.println("Enter the username");
                                String user = AppConst.scanner.next();
                                dao.listAllTasks(user);
                                logData.storeData("list Assign task method called");
                                break;
                            default:
                                System.out.println("wrong Option");
                                logData.storeData("User Enter wrong input");
                                break;
                    }
                    System.out.println("Do you want to continue");
                } while (AppConst.scanner.nextInt()!=0);
                break;
            case 2:
                System.out.println("ENTER THE NAME");
                name = AppConst.scanner.next();
                System.out.println("Enter Password");
                password = AppConst.scanner.next();
                if(clientType==1){
                    type = "client";
                }

                else {
                    type ="visitor";
                }
                userName = name;
                Register register = new Register();
                register.doRegister(name,password,type);
                break;
            default:
                System.out.println("Wrong Choice");
                System.exit(0);;
        }
        return client;

    }

    public static  void  visitor() throws InvalidUserException {
        System.out.println("1. Login");
        System.out.println("2. Register");
        System.out.println("Enter your choice");
        int choice = AppConst.scanner.nextInt();
        if(choice==1) {
            Login login = new Login();
            System.out.println("Enter Username");
            String name = AppConst.scanner.next();
            System.out.println("Enter Password");
            String password = AppConst.scanner.next();
            login.visitorLogin(name,password);
        }
        else if(choice==2) {
            Register register = new Register();
            System.out.println("Enter Username");
            String name = AppConst.scanner.next();
            System.out.println("Enter Password");
            String password = AppConst.scanner.next();
            String type = "visitor";
            register.visitorRegister(name,password,type);


        }
        else {
            System.out.println("Wrong Input");
            System.exit(0);
        }

        System.out.println("Do you want to see all task (Yes/No)");
        String  chr = AppConst.scanner.next();
        if(chr!="Yes"||chr!="yes"){
            System.out.println("Bye Bye");
        }
        TaskDaoImpl dao = new TaskDaoImpl();
        Tasks tasks1 = new Tasks();
        dao.listTasks();

    }

}
