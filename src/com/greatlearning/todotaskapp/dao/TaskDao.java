package com.greatlearning.todotaskapp.dao;

import com.greatlearning.todotaskapp.model.Tasks;

public interface TaskDao {
    public void add(long taskId, String taskTitle, String username);
    public void delete(long taskId);
    public void updateTask(long taskId);
    Tasks  searchTask(long taskId);
    public void assignTask(long taskId, String username);
    public void listTasks();
}
